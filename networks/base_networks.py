import tensorflow as tf
from util import data_batcher
import progressbar


def weights_variable(shape):
    initial = tf.truncated_normal(shape=shape, stddev=0.005)
    return tf.Variable(initial)

class BaseCRFDN(object):

    def __init__(self, input_max_len, tasks_count, embeddings):

        self.input_max_len = input_max_len
        self.tasks_count = tasks_count
        self.embeddings = tf.Variable(embeddings)

        self.input_sequence = tf.placeholder(dtype=tf.int32, shape=[None, None])
        self.input_lens = tf.placeholder(dtype=tf.int32, shape=[None])
        self.output_sequences = tf.placeholder(dtype=tf.int32, shape=(None, input_max_len))

    def _inference(self): raise NotImplementedError()

    def _losses(self):
        losses = []
        self.trans_params = []
        for task in range(self.tasks_count):
            with tf.variable_scope('task_{}_crf'.format(task)):
                log_likelihood, trans_params = tf.contrib.crf.crf_log_likelihood(self.logits[task], self.output_sequences, self.input_lens)
                losses.append(tf.reduce_mean(-log_likelihood))
                self.trans_params.append(trans_params)

        return losses

    def _optimizers(self):
        return [tf.train.AdamOptimizer(1e-3).minimize(self.losses[i]) for i in range(self.tasks_count)]

    def train_network(self, train_set, val_set, epochs, batch_size, callbacks=[]):

        """
            input is a sequence of word ids
            output is a sequence of label ids
        """
        batches_cnt, batcher = data_batcher(train_set, batch_size)
        pbar = progressbar.ProgressBar(maxval=batches_cnt, widgets=[
                                        progressbar.DynamicMessage('Epoch'),
                                        progressbar.Bar(),
                                        progressbar.AdaptiveETA(),
                                    ]
                                )

        for epoch in range(epochs):
            print 'EPOCH {}'.format(epoch+1)
            pbar.start()
            pbar.update(0, Epoch=epoch+1)
            batches = [batch for batch in batcher()]
            for batch_id in range(batches_cnt):
                task_id = batches[batch_id][3]
                input_seq = batches[batch_id][0]
                input_lens = batches[batch_id][2]
                output_seq = batches[batch_id][1]
                optimize_op = self.optimize_ops[task_id]
                loss = self.losses[task_id]
                feed_dict = self.check_feeds(
                                                task_id,
                                                input_seq,
                                                input_lens,
                                                output_seq
                                            )
                _, loss_values = self.sess.run([optimize_op, loss], feed_dict=feed_dict)

                pbar.update(batch_id, Epoch=epoch+1)


            pbar.finish()
            for callback in callbacks:
                callback(self, val_set)

    def predict(self, task_id, input_seq, input_lens):
        feed_dict = self.check_feeds(task_id, input_seq, input_lens)
        viterbi_sequences = []
        logits, trans_params = self.sess.run([self.logits[task_id], self.trans_params[task_id]], feed_dict=feed_dict)

        for logit, sequence_length in zip(logits, input_lens):
            logit = logit[:sequence_length]
            viterbi_seq, viterbi_score = tf.contrib.crf.viterbi_decode(logit, trans_params)
            viterbi_sequences += [viterbi_seq]

        return viterbi_sequences

    def bpredict(self, task_id, input_seq, input_lens, bsize=1024):
        datasize = len(input_seq)
        bcount = (datasize + bsize - 1)/bsize
        y_pred = []
        for bidx in range(bcount):
            bstart = bsize*bidx
            bend = min(datasize, bsize*(bidx+1))
            y_pred += self.predict(task_id, input_seq[bstart:bend], input_lens[bstart:bend])

        return y_pred

    def check_feeds(self, task_id, input_seq, input_lens, output_seq=None):
        return {
                    self.input_sequence:input_seq,
                    self.input_lens:input_lens,
                    self.output_sequences:output_seq
                } if output_seq is not None else \
                {
                    self.input_sequence:input_seq,
                    self.input_lens:input_lens,
                }

    def save(self, fpath): tf.train.Saver().save(self.sess, fpath)

    def restore_from_file(self, fpath): tf.train.Saver().restore(self.sess, fpath)


class BaseDN(object):

    def __init__(self, input_max_len, tasks_count, embeddings):
        self.input_max_len = input_max_len
        self.tasks_count = tasks_count
        self.embeddings = tf.Variable(embeddings)

        self.input_sequence = tf.placeholder(dtype=tf.int32, shape=(None, None))
        self.input_lens = tf.placeholder(dtype=tf.int32, shape=(None))
        self.output_sequences = tf.placeholder(dtype=tf.int32, shape=(None, input_max_len))

    def _inference(self): raise NotImplementedError()

    def _losses(self):
        losses_on_batch = [
                            tf.nn.sparse_softmax_cross_entropy_with_logits(
                                        labels=self.output_sequences,
                                        logits=self.logits[i]
                            )
                            for i in range(self.tasks_count)
                        ]
        return [tf.reduce_mean(loss) for loss in losses_on_batch]

    def _optimizers(self):
        return [tf.train.AdamOptimizer(1e-2).minimize(self.losses[i]) for i in range(self.tasks_count)]

    def train_network(self, train_set, val_set, epochs, batch_size, callbacks=[]):
        """
            input is a sequence of word ids
            output is a sequence of label ids
        """

        for epoch in range(epochs):
            batches_cnt, batcher = data_batcher(train_set, batch_size)
            pbar = progressbar.ProgressBar(maxval=batches_cnt, widgets=[
                                        progressbar.DynamicMessage('Epoch'),
                                        progressbar.Bar(),
                                        progressbar.AdaptiveETA(),
                                    ]
                                )
            batches = [batch for batch in batcher()]
            progress = 0
            pbar.start()
            pbar.update(0, Epoch=epoch+1)
            for batch_id in range(batches_cnt):
                task_id = batches[batch_id][3]
                input_seq = batches[batch_id][0]
                input_lens = batches[batch_id][2]
                output_seq = batches[batch_id][1]
                optimize_op = self.optimize_ops[task_id]
                loss = self.losses[task_id]
                feed_dict = self.check_feeds(
                                                task_id,
                                                input_seq,
                                                input_lens,
                                                output_seq
                                            )
                _, loss_values = self.sess.run(
                                            [optimize_op, loss],
                                            feed_dict=feed_dict
                                        )
                pbar.update(progress, Epoch=epoch+1)
                progress += 1

            pbar.finish()
            for callback in callbacks:
                callback(self, val_set)

    def predict(self, task_id, input_seq, input_lens):
        feed_dict = self.check_feeds(task_id, input_seq, input_lens)
        return self.sess.run(self.predict_ops[task_id], feed_dict=feed_dict)


    def check_feeds(self, task_id, input_seq, input_lens, output_seq=None):
        return {
                    self.input_sequence:input_seq,
                    self.input_lens:input_lens,
                    self.output_sequences:output_seq
                } if output_seq is not None else \
                {
                    self.input_sequence:input_seq,
                    self.input_lens:input_lens,
                }

    def save(self, fpath):
        pass

    def resrore_from_file(self, fpath):
        pass


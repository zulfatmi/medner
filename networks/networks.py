from base_networks import *


def get_rnn_lstm(num_layers, hidden_units):
    create_cell = tf.contrib.cudnn_rnn.CudnnCompatibleLSTMCell
    return tf.contrib.rnn.MultiRNNCell([
                                            create_cell(hidden_units)
                                            for _ in range(num_layers)
                                        ]), \
           tf.contrib.rnn.MultiRNNCell([
                                            create_cell(hidden_units)
                                            for _ in range(num_layers)
                                        ])


class LSTMCRF(BaseCRFDN):

    def __init__(self, layers, input_max_len, tasks_count, embeddings, hidden_units, classes_counts):
        super(LSTMCRF, self).__init__(input_max_len, tasks_count, embeddings)
        self.classes_counts = classes_counts
        self.hidden_units = hidden_units
        self.layers = layers

        self.bidir_lstm = get_rnn_lstm(layers, hidden_units)

        self.W = [weights_variable([2*hidden_units, classes_count]) for classes_count in classes_counts]
        self.b = [weights_variable([classes_count]) for classes_count in classes_counts]

        self.logits = self._inference()
        self.losses = self._losses()
        self.optimize_ops = self._optimizers()
        self.predict_ops = [tf.argmax(tslogits, axis=-1) for tslogits in self.logits]
        gpu_options = tf.GPUOptions(allow_growth=True)
        self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, gpu_options=gpu_options))
        self.sess.run(tf.global_variables_initializer())

    def _inference(self):
        with tf.name_scope('embedings'):
            embedded_sequence = tf.nn.embedding_lookup(self.embeddings, self.input_sequence)

        with tf.name_scope('lstm'):
            rnn_outputs, states = tf.nn.bidirectional_dynamic_rnn(self.bidir_lstm[0], self.bidir_lstm[1], embedded_sequence, sequence_length=self.input_lens, dtype=tf.float32)

        rnn_outputs = tf.concat(rnn_outputs, axis=2)
        rnn_outputs = tf.reshape(rnn_outputs, [-1, 2*self.hidden_units])

        rnn_outputs = tf.nn.dropout(rnn_outputs, keep_prob=0.8)
        logits = [tf.matmul(rnn_outputs, self.W[i]) + self.b[i] for i in range(self.tasks_count)]
        logits = [tf.reshape(tslogits, [-1, self.input_max_len, classes_count]) for tslogits, classes_count in zip(logits, self.classes_counts)]
        return logits


class LSTM(BaseDN):

    def __init__(self, input_max_len, tasks_count, embeddings, hidden_units, classes_counts):
        super(LSTM, self).__init__(input_max_len, tasks_count, embeddings)
        self.classes_counts = classes_counts
        self.hidden_units = hidden_units

        self.lstm_fw = tf.contrib.cudnn_rnn.CudnnLSTM(num_layers=4, num_units=hidden_units, direction='bidirectional')

        self.W = [weights_variable([2*hidden_units, classes_count]) for classes_count in classes_counts]
        self.b = [weights_variable([classes_count]) for classes_count in classes_counts]

        self.logits = self._inference()
        self.losses = self._losses()
        self.optimize_ops = self._optimizers()
        self.predict_ops = [tf.argmax(tslogits, axis=-1) for tslogits in self.logits]
        self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=True))
        self.sess.run(tf.global_variables_initializer())

    def _inference(self):
        with tf.name_scope('embedings'):
            embedded_sequence = tf.nn.embedding_lookup(self.embeddings, self.input_sequence)

        with tf.name_scope('lstm'):
            rnn_outputs, states = self.lstm_fw(embedded_sequence)

        rnn_outputs = tf.concat(rnn_outputs, axis=2)
        rnn_outputs = tf.reshape(rnn_outputs, [-1, 2*self.hidden_units])

        logits = [tf.matmul(rnn_outputs, self.W[i]) + self.b[i] for i in range(self.tasks_count)]
        logits = [tf.reshape(tslogits, [-1, self.input_max_len, classes_count]) for tslogits, classes_count in zip(logits, self.classes_counts)]
        return logits

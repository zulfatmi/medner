import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from util import data_batcher
from time import time
import progressbar




class BiLSTM(nn.Module):

    def __init__(self, input_max_len, tasks_count, embeddings, hidden_units=100, classes_counts=[5]):
        super(BiLSTM, self).__init__()
        self.vocab_size, self.embedding_dim = embeddings.shape
        self.hidden_units = hidden_units
        self.classes_counts = classes_counts
        self.input_max_len = input_max_len
        self.rnn_layers = 1

        self.embeddings = nn.Embedding(self.vocab_size, self.embedding_dim)
        self.lstm = nn.LSTM(self.embedding_dim, self.hidden_units // 2,
                            num_layers=self.rnn_layers, bidirectional=True)
        self.linear_map = nn.Linear(hidden_units, self.classes_counts[0])


    def forward(self, input):
        self.init_hidden(input.shape[1])
        embedded = self.embeddings(input)
        lstm_out, self.hidden = self.lstm(embedded, self.hidden)
        lmap_out = self.linear_map(lstm_out)
        return F.softmax(lmap_out, dim=-1)

    def train_network(self, train_set, val_set, epochs, batch_size, callbacks=[], savepath='models/nw_epoch_{}'):
        optimizer = optim.Adam(self.parameters(), lr=0.01)
        criterion = nn.CrossEntropyLoss()

        batches_cnt, batcher = data_batcher(train_set, batch_size)
        batches = [batch for batch in batcher()]
        batches_inputs = [to_var(batch[0]) for batch in batches]
        batches_outputs = [autograd.Variable(torch.LongTensor(batch[1])).view(-1) for batch in batches]

        pbar = progressbar.ProgressBar(maxval=batches_cnt, widgets=[
                                        progressbar.DynamicMessage('Epoch'),
                                        progressbar.Bar(),
                                        progressbar.AdaptiveETA(),
                                    ]
                                )
        for epoch in range(epochs):
            pbar.start()
            pbar.update(0, Epoch=epoch+1)
            for i in range(batches_cnt):
                pbar.update(i, Epoch=epoch+1)
                self.zero_grad()
                logits = self.forward(batches_inputs[i])
                logits = logits.view(batch_size*self.input_max_len, -1)
                loss = criterion(logits, batches_outputs[i])
                loss.backward()
                optimizer.step()
            pbar.finish()

    def init_hidden(self, batch_size):
        self.hidden = autograd.Variable(torch.randn(2*self.rnn_layers, batch_size, self.hidden_units/2)),\
            autograd.Variable(torch.randn(2*self.rnn_layers, batch_size, self.hidden_units/2))


    def predict(self):pass
    def save(self):pass
    def load(self):pass


def to_var(input):
    return autograd.Variable(torch.LongTensor(input).transpose(0,1))

import numpy as np
from networks.networks import LSTMCRF
from util import config_from_file, DataProcessor, get_embeddings, pad_sequence
import tensorflow as tf
import argparse
import codecs
from nltk.tokenize import wordpunct_tokenize


if __name__ == '__main__':
    bsize = 1024
    tasks = [0, 1]
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', dest='path_to_model')
    parser.add_argument('--mconf', dest='path_to_mconf')
    parser.add_argument('--in_vocab', dest='path_to_input_vocab')
    parser.add_argument('--out_vocabs', dest='path_to_output_vocabs')
    parser.add_argument('--input', dest='input_file')
    parser.add_argument('--output', dest='output_file')
    args = parser.parse_args()
    config = config_from_file(args.path_to_mconf)

    dp = DataProcessor()
    dp.load_input_vocab(args.path_to_input_vocab)
    dp.load_output_vocabs(args.path_to_output_vocabs)
    embeddings = get_embeddings(dp.input_vocab)
    dp.reverse_vocabularies()
    model = LSTMCRF(
                    layers=config['layers'],
                    input_max_len=config['input_max_len'],
                    tasks_count=config['tasks_count'],
                    embeddings=embeddings,
                    hidden_units=config['hidden_units'],
                    classes_counts=config['classes_counts'])
    model.restore_from_file(args.path_to_model)

    with codecs.open(args.input_file) as input_file:
        input_texts = input_file.readlines()

    input_texts = map(wordpunct_tokenize, input_texts)

    input_seq = dp.map_insequences(input_texts, update=False)
    input_seq = np.array(map(lambda seq: pad_sequence(seq, 473), input_seq))
    input_seq_lens = map(len, input_seq)
    input_seq_lens = map(lambda length: length if length < 473 else 473, input_seq_lens)
    examples_cnt = len(input_seq)

    with codecs.open(args.output_file, 'w') as output_file:
        for task in tasks:
            output_seq_t = model.bpredict(
                                            task,
                                            input_seq,
                                            input_seq_lens,
                                        )
            output_seq_t = dp.map_back(output_seq_t, str(task))
            output_seq = [['{}\t{}'.format(out_seq, out_seq_t)
                            for out_seq, out_seq_t in zip(out_seq_, out_seq_t_)]
                            for out_seq_, out_seq_t_ in zip(output_seq, output_seq_t)]

        for in_seq_, out_seq_ in zip(input_seq, output_seq):
            for in_seq, out_seq in zip(in_seq_, out_seq_):
                output_file.write('{}\t{}\n'.format(dp.reversed_input_vocab[in_seq], out_seq))
            output_file.write('\n\n')


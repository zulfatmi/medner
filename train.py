from util import DataProcessor, data_batcher, read_datasets, get_embeddings,pad_sequences, truncate_lens, evaluate, save_config
from networks.networks import LSTMCRF, LSTM
from gensim.models import KeyedVectors
from metrics import exact_matching
import tensorflow as tf
import argparse
import json
import os

config = {}

def train(dss_folder, w2v_path, tasks_count, hidden_units, bsize, epochs, layers, device):
    with tf.device(device):
        dp = DataProcessor()
        dataset_folders = ['dataset_{}'.format(task_id) for task_id in range(tasks_count)]
        paths_to_train_datasets = [os.path.join(dss_folder, ds_folder, 'train.tsv') for ds_folder in dataset_folders]
        paths_to_val_datasets = [os.path.join(dss_folder, ds_folder, 'test.tsv') for ds_folder in dataset_folders]

        train_datasets = read_datasets(paths_to_train_datasets)
        input_max_len = 0
        for ds in train_datasets:
            task_id = ds[0]
            ds[1] = dp.map_insequences(ds[1])
            ds[2] = dp.map_outsequences(ds[2], task_id)
            ds_input_max_len = max(ds[3])
            if input_max_len < ds_input_max_len: input_max_len = ds_input_max_len
        config['input_max_len'] = input_max_len

        for ds in train_datasets: pad_sequences(ds, input_max_len)
        for ds in train_datasets: truncate_lens(ds, input_max_len)

        w2v_model = {}
        if w2v_path is not None:
            w2v_model = KeyedVectors.load_word2vec_format(w2v_path, binary=True)
        embeddings = get_embeddings(dp.input_vocab, w2v_model=w2v_model)

        dp.reverse_vocabularies()

        val_datasets = read_datasets(paths_to_val_datasets)
        for ds in val_datasets: ds[1] = dp.map_insequences(ds[1], update=False)
        for ds in val_datasets: pad_sequences(ds, input_max_len)
        for ds in val_datasets: truncate_lens(ds, input_max_len)

        classes_counts = [len(dp.output_vocabs[task_id]) for task_id in range(tasks_count)]
        config['classes_counts'] = classes_counts

        dp.save_vocabs(
                            input_vocab_path='models/vocabs/input_vocab',
                            output_vocab_path='models/vocabs/output_vocabs'
                        )
        save_config('models/config', config)
        network = LSTMCRF(
                            layers=layers,
                            input_max_len=input_max_len,
                            tasks_count=tasks_count,
                            embeddings=embeddings,
                            hidden_units=hidden_units,
                            classes_counts=classes_counts
                      )

        network.train_network(train_set=train_datasets, val_set=val_datasets, epochs=args.epochs, batch_size=args.bsize, callbacks=[lambda net, dataset: evaluate(net, dataset, exact_matching, dp)])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dss_folder', dest='dss_folder', default='')
    parser.add_argument('--tasks_count', dest='tasks_count', type=int, default=1)
    parser.add_argument('--w2v', dest='w2v_path', default=None)
    parser.add_argument('--epochs', dest='epochs', type=int, default=70)
    parser.add_argument('--bsize', dest='bsize', type=int, default=32)
    parser.add_argument('--hunits', dest='hidden_units', type=int, default=150)
    parser.add_argument('--layers', dest='layers', type=int, default=1)
    parser.add_argument('--device', dest='device', default='/device:GPU:0')
    args = parser.parse_args()
    config = vars(args)

    train(args.dss_folder, args.w2v_path, args.tasks_count, args.hidden_units, args.bsize, args.epochs, args.layers, args.device)





def exact_matching(y_true, y_pred, iob_scheme):
    """
        y_true and y_pred must be in IOB scheme
        calculates:
                precision = true_pos/(true_pos + false_pos)
                recall = true_pos/(true_pos + false_neg)
                f_measure = 2*P*F/(P+F)
    """

    estart = iob_scheme['ESTART']
    ein = iob_scheme['EIN']
    eout = iob_scheme['EOUT']

    true_pred_entities_count = 0
    true_entities_count = 0
    pred_entities_count = 0
    matching = False
    for true_labels, pred_labels in zip(y_true, y_pred):
        for true_label, pred_label in zip(true_labels, pred_labels):
            is_true_entity_started = true_label == estart
            is_pred_entity_started = pred_label == estart
            is_true_entity_ended = true_label == eout or is_true_entity_started
            is_pred_entity_ended = pred_label == eout or is_pred_entity_started

            true_entities_count += is_true_entity_started
            pred_entities_count += is_pred_entity_started


            if is_true_entity_ended and is_pred_entity_ended and matching:
                matching = False
                true_pred_entities_count += 1
            else:
                matching = matching and true_label == pred_label

            matching = matching or is_pred_entity_started and is_true_entity_started


    precision = 1.0*true_pred_entities_count/pred_entities_count if pred_entities_count != 0 else 0
    recall = 1.0*true_pred_entities_count/true_entities_count if true_entities_count != 0 else 0
    f_measure = 2*precision*recall/(precision + recall) if precision + recall != 0 else 0

    return precision, recall, f_measure


def partial_matching(y_true, y_pred):

    true_pred_entities_count = 0
    true_entities_count = 0
    pred_entities_count = 0

    for true_labels, pred_labels in zip(y_true, y_pred):
        for true_label, pred_label in zip(true_labels, pred_labels):
            pass

    return None, None, None



metrics = {
            'exact_matching':exact_matching,
            'partial_matching':partial_matching,
        }
